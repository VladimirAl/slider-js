const slides = document.querySelectorAll(".slide");

const leftArrow = document.querySelector(".left-arrow");
const rightArrow = document.querySelector(".right-arrow");

function swipeRight() {
  for (let i = 0; i < slides.length; i++) {
    if (slides[i].classList.contains("active")) {
      if (i < slides.length - 1) {
        slides[i].classList.remove("active");
        slides[i + 1].classList.add("active");
        break;
      } else if (i === slides.length - 1) {
        slides[slides.length - 1].classList.remove("active");
        slides[0].classList.add("active");
        break;
      }
    }
  }
}

rightArrow.addEventListener("click", swipeRight);

function swipeLeft() {
  for (let i = slides.length - 1; i >= 0; i--) {
    if (slides[i].classList.contains("active")) {
      if (i > 0) {
        console.log(1);
        slides[i].classList.remove("active");
        slides[i - 1].classList.add("active");
        break;
      } else if (i === 0) {
        console.log(2);
        slides[0].classList.remove("active");
        slides[slides.length - 1].classList.add("active");
        break;
      }
    }
  }
}

leftArrow.addEventListener("click", swipeLeft);

for (const slide of slides) {
  slide.addEventListener("click", () => {
    clearActiveClasses();

    slide.classList.add("active");
  });
}

function clearActiveClasses() {
  slides.forEach((slide) => {
    slide.classList.remove("active");
  });
}
